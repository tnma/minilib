def print_string(m, n):
    print(m * n)


def biggest(n1, n2):
    return n1 if n1 > n2 else n2


def mirror(n):
    return int(str(n)[::-1])


def swap_last_first(n):
    return int(str(n)[-1] + str(n)[1:-1] + str(n)[0])


def is_palindrome(n):
    return str(n) == str(n)[::-1]


def factorial(n):
    return reduce(lambda x, y: x * y, [1] + range(1, n + 1))


def sum_list(l):
    return reduce(lambda x, y: x + y, l)


def smallest(*l):
    return reduce(lambda x, y: x if x < y else y, l)


def union(l1, l2):
    return list(set(l1) & set(l2))


def make_odds(n, i):
    return range(i, i + (n * 2))[::2]


def fib(n):
    # http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibFormula.html
    return int(((1 + 2.2360679775)**n - (1 - 2.2360679775)**n) / (2**n * 2.2360679775))


# testing the functions
# print_string("test", 5)
# print swap_last_first(12345)
# print is_palindrome(12344321)
# print factorial(0)
# print sum_list([1, 2, 3, 4])
# print smallest(4, 2, 3, 4)
# print union([1, 2, 3], [2, 3, 4])
# print make_odds(5, 11)
# print fib(11)
